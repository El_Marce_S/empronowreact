import i18next from 'i18next';
import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
	{
		id: 'applications',
		title: 'Applications',
		translate: 'APPLICATIONS',
		type: 'group',
		icon: 'apps',
		children: [
			{
				id: 'example-component',
				title: 'Example',
				translate: 'EXAMPLE',
				type: 'item',
				icon: 'whatshot',
				url: '/example'
			},
			{
				id: 'example-component',
				title: 'Example',
				translate: 'EXAMPLE',
				type: 'item',
				icon: 'whatshot',
				url: '/Test1'
			},
			{
				id: 'example-component2',
				title: 'Test',
				translate: 'EXAMPLE',
				type: 'item',
				icon: 'Home',
				url: '/TEST2'
			}
		]

	},
	{
		id: 'login-v2',
		title: 'Login v2',
		type: 'item',
		url: '/pages/auth/login-2'
	},
];

export default navigationConfig;
